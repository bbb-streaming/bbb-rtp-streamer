import json
import requests_mock
import unittest
from unittest.mock import patch, MagicMock, Mock

from stream_queue import Streamer, Worker


class WorkerTestOnJobMessage(unittest.TestCase):
    def test_on_job_message_ok(self):
        w = Worker("url", "uuid")
        w.unregister_jobs_channel = MagicMock()
        w.register_jobs_channel = MagicMock()
        w.handle_stream_job = MagicMock(return_value=True)
        w.on_job_message("channel", "meth", "prop", "body")
        w.unregister_jobs_channel.assert_called()
        w.register_jobs_channel.assert_not_called()
        w.handle_stream_job.assert_called_with("body")
        self.assertEqual(w.state, Worker.STREAMING)

    def test_on_job_message_fail(self):
        w = Worker("url", "uuid")
        w.unregister_jobs_channel = MagicMock()
        w.register_jobs_channel = MagicMock()
        w.handle_stream_job = MagicMock(return_value=False)
        w.on_job_message("channel", "meth", "prop", "body")
        w.unregister_jobs_channel.assert_called()
        w.register_jobs_channel.assert_called()
        w.handle_stream_job.assert_called_with("body")
        self.assertEqual(w.state, Worker.WAITING)


class WorkerTestOnJobMessage(unittest.TestCase):
    def setUp(self):
        self.w = Worker("url", "uuid")
        self.join_url = 'mock://join'
        self.running_url = 'mock://running'
        self.jobdata = json.dumps({
            'meeting_running_url': self.running_url,
            'meeting_join_url': self.join_url,
            'janus_host': 'janus',
            'audio_port': 1,
            'video_port_hq': 2,
            'video_port_mq': 3,
            'video_port_lq': 4,
        })

    def test_invalid_json(self):
        ret = self.w.handle_stream_job("{}}")
        self.assertFalse(ret)

    def test_invalid_job_data(self):
        with patch.object(self.w, "validate_job_data", return_value=False) as m:
            ret = self.w.handle_stream_job("{}")
            m.assert_called()
            self.assertFalse(ret)

    def test_meeting_running_no(self):
        with requests_mock.Mocker() as m:
            m.get('mock://running', text="""<response>
<returncode>SUCCESS</returncode>
<running>false</running>
</response>""", status_code=200)
            ret = self.w.handle_stream_job(self.jobdata)
            self.assertFalse(ret)
        with requests_mock.Mocker() as m:
            m.get('mock://running', text='garbage', status_code=200)
            ret = self.w.handle_stream_job(self.jobdata)
            self.assertFalse(ret)


    def test_meeting_running_api_fail(self):
        pass

    def test_meeting_running_yes(self):
        with requests_mock.Mocker() as m:
            m.get('mock://running', text="""<response>
<returncode>SUCCESS</returncode>
<running>true</running>
</response>""", status_code=200)
            with patch('stream_queue.Streamer', autospec=True) as s:
                ret = self.w.handle_stream_job(self.jobdata)
                s.assert_called()
                self.w.streamer.start_browser.assert_called()
                self.w.streamer.join_bbb.assert_called_with(self.join_url)
                self.w.streamer.start_streaming.assert_called()
                self.assertEqual(self.w.streamer.janus_host, 'janus')
                self.assertEqual(self.w.streamer.audio_port, 1)
                self.assertEqual(self.w.streamer.video_port_hq, 2)
                self.assertEqual(self.w.streamer.video_port_mq, 3)
                self.assertEqual(self.w.streamer.video_port_lq, 4)
            self.assertTrue(ret)


class WorkerTestTerminateStream(unittest.TestCase):
    def test_no_stream(self):
        w = Worker("url", "uuid")
        w.streamer = Mock()
        w.streamer.__bool__ = Mock(return_value=False)
        w.streamer.stop_streaming = Mock()
        w.streamer.stop_browser = Mock()
        w.terminate_stream()
        w.streamer.stop_streaming.assert_not_called()
        w.streamer.stop_browser.assert_not_called()
        
    def test_stream(self):
        w = Worker("url", "uuid")
        stream_mock = Mock()
        w.streamer = stream_mock
        w.streamer.__bool__ = Mock(return_value=True)
        w.streamer.stop_streaming = Mock()
        w.streamer.stop_browser = Mock()
        w.terminate_stream()
        stream_mock.stop_streaming.assert_called()
        stream_mock.stop_browser.assert_called()
        self.assertEqual(w.streamer, None)

if __name__ == '__main__':
    unittest.main()
