#!/usr/bin/env python3

import os
import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib, GObject

import subprocess
import shlex
import argparse
import time

# http://docs.gstreamer.com/display/GstSDK/Basic+tutorial+3%3A+Dynamic+pipelines

def is_port_listening(port_number):
    first_done = False
    with open("/proc/net/tcp") as f:
        for line in f.readlines():
            if not first_done:
                first_done = True
                continue
            parts = line.strip().split(" ")
            local_part = parts[1]
            ip, port = local_part.split(":")
            port = int(port, 16)
            state = int(parts[3], 16)
            if port_number == port:
                if state == 10:
                    return True
                else:
                    return False
    return False


def ssrc_from_clients(clients):
    return clients.split(",")[0].split(":")[1]


class Player(object):

    def __init__(self, args):
        self.args = args
        self.ffmpeg_launches_tried = 0
        self.launch_ffmpeg()
        # initialize GStreamer
        Gst.init(None)
        self.loop = GLib.MainLoop()
        self.prerolled = False
        # create the elements
        self.source = Gst.ElementFactory.make("tcpclientsrc", "source")
        self.demux = Gst.ElementFactory.make("matroskademux", "demux")
        
        audiopipe = "{}\n{}".format(
                self.audio_send_fragment(),
                self.pulseinput_fragment()
            )
        print("audio: %s" % audiopipe)
        self.audiobin = Gst.parse_bin_from_description(
            "{}\n{}".format(
                self.audio_send_fragment(),
                self.pulseinput_fragment()
            ),
            True
        )
        self.audioqueue = self.audiobin.get_by_name("audioqueue")
        self.audioselect = self.audiobin.get_by_name("audioselect")
        
        videopipe = "{}\n{}".format(self.video_send_fragment(), self.xinput_fragment())
        print("videopipe: %s" % videopipe)
        self.videobin = Gst.parse_bin_from_description(
            "{}\n{}".format(self.video_send_fragment(), self.xinput_fragment()),
            True
        )
        self.videoselect = self.videobin.get_by_name("videoselect")

        # create empty pipeline
        self.pipeline = Gst.Pipeline.new("test-pipeline")

        if not self.pipeline or not self.source or not self.audiobin or not self.videobin:
            print("ERROR: Could not create all elements")
            sys.exit(1)

        # build the pipeline. we are NOT linking the source at this point.
        # will do it later
        self.pipeline.add(self.source)
        self.pipeline.add(self.demux) 
        self.pipeline.add(self.audiobin)
        self.pipeline.add(self.videobin)

        # wait for ffmpeg to listen
        while not is_port_listening(10000):
            time.sleep(1)
            ffmpeg_status = self.ffmpeg.poll()
            if ffmpeg_status:
                # ffmpeg has died
                if self.ffmpeg_launches_tried > 3:
                    raise RuntimeError("could not launch ffmpeg")
                self.ffmpeg.wait()
                self.launch_ffmpeg()
    
        # set the URI to play
        self.source.set_property("host", "127.0.0.1")
        self.source.set_property("port", 10000)
        self.source.set_property("do-timestamp", True)
        self.source.link(self.demux)

        # connect to the pad-added signal
        self.demux.connect("pad-added", self.on_pad_added)

        self.pipeline.use_clock(Gst.SystemClock.obtain())

        bus = self.pipeline.get_bus()
        bus.connect('message::eos', self.on_eos)
        bus.connect('message::error', self.on_error)
        bus.connect('message::state-changed', self.on_state_changed)
        print("init completed")

    def launch_ffmpeg(self):
        self.ffmpeg = subprocess.Popen(
            shlex.split("""ffmpeg -y -nostdin -fflags +genpts -stream_loop -1 -i""") +
            [self.args.pause_file] +
            shlex.split("""-ac 2 -filter_complex "[0:v] scale=1920:1080,fps=30 [v] ; [0:a] aresample=48000 [a]" -map "[v]" -map "[a]" -pix_fmt yuv420p -c:v rawvideo -c:a pcm_s16le -f matroska tcp://127.0.0.1:10000?listen=1"""),
            stdin=subprocess.DEVNULL
        )
        self.ffmpeg_launches_tried += 1

    def xinput_fragment(self):
        return "ximagesrc do-timestamp=true use-damage=false display-name=:{} show-pointer=false ! video/x-raw,framerate=30/1 ! queue max-size-time=3000000000 ! videoselect.".format(122)

    def pulseinput_fragment(self):
        return "pulsesrc do-timestamp=true ! audioresample ! audio/x-raw,channels=2,rate=48000 ! queue max-size-time=3000000000 ! audioselect."

    def audio_send_fragment(self):
        return "queue max-size-time=3000000000 name=audioqueue ! audioresample ! audio/x-raw,channels=2,rate=48000 ! queue max-size-time=300000000 ! audiomixer name=audioselect ! audio/x-raw,channels=2,rate=48000 ! queue max-size-time=300000000 ! opusenc bitrate=128000 ! rtpopuspay ssrc={} ! multiudpsink sync=true async=false clients={}".format(
            ssrc_from_clients(self.args.audio),
            self.args.audio
        )

    def video_send_fragment(self):
        def video_fragment(xres, yres, bitrate, threads):
            s = "queue max-size-time=3000000000 ! videoscale ! video/x-raw,width={},height={} ! videoconvert ! videorate ! video/x-raw,framerate=30/1 ! queue max-size-time=3000000000 "
            s += " ! vp8enc undershoot=95 error-resilient=1 deadline=1 cpu-used=16 end-usage=cbr target-bitrate={} keyframe-max-dist=90 token-partitions=3 dropframe-threshold=25 threads={} "
            return s.format(xres, yres, bitrate, threads)

        video = "queue max-size-time=3000000000 name=videoqueue ! videomixer name=videoselect ! tee name=t ! {} ! rtpvp8pay ssrc={} pt=100 mtu=1200 picture-id-mode=2 ! queue max-size-time=3000000000 ! multiudpsink sync=true clients={}".format(
            video_fragment(960, 540, 1000000, 4),
            ssrc_from_clients(self.args.video_lq),
            self.args.video_lq
        )
        if self.args.video_mq:
            video = "{} t. ! {} ! rtpvp8pay ssrc={} pt=100 mtu=1200 picture-id-mode=2 ! queue max-size-time=3000000000 ! multiudpsink sync=true clients={}".format(
                video,
                video_fragment(1280, 720, 2000000, 4),
                ssrc_from_clients(self.args.video_mq),
                self.args.video_mq
            )
        if self.args.video_hq:
            video = "{} t. ! {} ! rtpvp8pay ssrc={} pt=100 mtu=1200 picture-id-mode=2 ! queue max-size-time=3000000000 ! multiudpsink sync=true clients={}".format(
                video,
                video_fragment(1920, 1080, 3000000, 16) + " resize-allowed=true",
                ssrc_from_clients(self.args.video_hq),
                self.args.video_hq
            )
        return video

    # handler for the pad-added signal
    def on_pad_added(self, src, new_pad):
        print(
            "Received new pad '{0:s}' from '{1:s}'".format(
                new_pad.get_name(),
                src.get_name()))


        # check the new pad's type
        new_pad_caps = new_pad.get_current_caps()
        new_pad_struct = new_pad_caps.get_structure(0)
        new_pad_type = new_pad_struct.get_name()
        if new_pad_type.startswith("audio/x-raw"):
            sink_pad = self.audiobin.get_static_pad("sink")
        elif new_pad_type.startswith("video/x-raw"):
            sink_pad = self.videobin.get_static_pad("sink")
        else:
            print("pad has unknown type '{0:s}' -> ignored".format(new_pad_type))
            return
        if(sink_pad.is_linked()):
            print("We are already linked. Ignoring.")
            return

        # attempt the link
        # if our converter is already linked, we have nothing to do here
        ret = new_pad.link(sink_pad)
        if not ret == Gst.PadLinkReturn.OK:
            print("Type is '{0:s}}' but link failed".format(new_pad_type))
        else:
            print("Link succeeded (type '{0:s}')".format(new_pad_type))

        return

    def source_bus_call(self, bus, msg, *args):
        print("source_bus_call: {}".format(msg))
        if msg.type == Gst.MessageType.ASYNC_DONE:
            if not self.prerolled:
                print("Initial seek ...")
                flags = Gst.SeekFlags.FLUSH | Gst.SeekFlags.SEGMENT
                self.source.seek_simple(Gst.Format.TIME, flags, 0)
                self.prerolled = True
        elif msg.type == Gst.MessageType.SEGMENT_DONE:
            print("Looping...")
            flags = Gst.SeekFlags.SEGMENT
            self.source.seek_simple(Gst.Format.TIME, flags, 0)
        elif msg.type == Gst.MessageType.ERROR:
            print(msg.parse_error())
            self.loop.quit()            # quit.... (better restart app?)
        return True

    def on_switch(self, pad):
        audio_pad0 = self.audioselect.get_static_pad("sink_0")
        video_pad0 = self.videoselect.get_static_pad("sink_0")
        audio_pad1 = self.audioselect.get_static_pad("sink_1")
        video_pad1 = self.videoselect.get_static_pad("sink_1")
        if pad.strip() == 'quit':
            self.quit()
        elif pad.strip() == 'play':
            audio_pad0.set_property('mute', True)
            audio_pad1.set_property('mute', False)
            video_pad0.set_property("zorder", 0)
            video_pad1.set_property("zorder", 1)
        else:
            audio_pad0.set_property('mute', False)
            audio_pad1.set_property('mute', True)
            video_pad0.set_property("zorder", 1)
            video_pad1.set_property("zorder", 0)

    def quit(self):
        self.pipeline.set_state(Gst.State.NULL)
        if self.ffmpeg:
            self.ffmpeg.terminate()
            self.ffmpeg.kill()
        self.loop.quit()


    def on_eos(self, bus, msg):
        self.quit()

    def on_error(self, bus, msg):
        (err, debug) = msg.parse_error()
        print("error: %s" % err)
        self.quit()

    def on_state_changed(self, bus, msg):
        if msg.src != self.pipeline:
            print("state changed: %s" % msg.src.get_name() )
            return
        old_state, new_state, pending = msg.parse_state_changed()
        print("%s from %s to %s" % (msg.src.get_name(), old_state, new_state))

    def run(self):
        self.pipeline.set_state(Gst.State.PLAYING)
        self.on_switch("pause")
        print("pipeline set to state playing")
        self.loop.run()
        self.ffmpeg.terminate()
        self.ffmpeg.kill()

if __name__ == '__main__':
    print("ENV: %s", os.environ)
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--audio', required=True, help="a comma seperated list of host:port where audio stream is sent")
    parser.add_argument('-hq', '--video-hq', required=True, help="a comma seperated list of host:port where hq video stream is sent")
    parser.add_argument('-mq', '--video-mq', required=True, help="a comma seperated list of host:port where mq video stream is sent")
    parser.add_argument('-lq', '--video-lq', required=True, help="a comma seperated list of host:port where lq video stream is sent")
    parser.add_argument('-p', '--pause-file', required=True)
    args = parser.parse_args()

    p = Player(args)
    def stdin_cb(source, condition):
        content = source.readline()
        print("stdin received: %s" % content)
        p.on_switch(content)
        return True
    GLib.io_add_watch(sys.stdin, GLib.IO_IN, stdin_cb)
    p.run()
