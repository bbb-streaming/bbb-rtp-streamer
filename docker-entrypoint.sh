#!/bin/sh

pulseaudio --start -vvv --disallow-exit --log-target=syslog --high-priority --exit-idle-time=-1 --daemonize
pacmd load-module module-null-sink sink_name=default rate=48000

exec "$@"
