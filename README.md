# BBB Live Streaming

Streams a given BBB Meeting to an RTP Server.

## CLT Anpassung

### Build

```
docker build -t bbb-rtp-streamer .
```

### Environment

* AMQP_URI: RabbitMQ Connect URI (required)
* WORKER_ID: Worker UUID (required)

