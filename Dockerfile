FROM debian:bullseye

WORKDIR /usr/src/app

RUN apt-get update && DEBIAN_FRONTEND="noninteractive" apt-get install -y \
        libgstreamer1.0-0 gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio \
        software-properties-common \
        python3-pip \
        python3-gst-1.0 \
        python3-dev \
        xvfb \
        fluxbox \
        dbus-x11 \
        libasound2 \
        libasound2-plugins\
        alsa-utils \
        alsa-oss \
        ffmpeg \
        pulseaudio \
        pulseaudio-utils \
        gnupg \
        wget \
        curl \
        unzip \
        tzdata

RUN ln -s /usr/bin/python3 /usr/local/bin/python \
    && pip3 install --upgrade pip

COPY py_requirements.txt ./

RUN pip install --no-cache-dir -r py_requirements.txt

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list && \
    apt-get update -y && \
    apt-get install -y google-chrome-stable && \
    CHROMEVER=$(google-chrome --product-version | grep -o "[^\.]*\.[^\.]*\.[^\.]*") && \
    DRIVERVER=$(curl -s "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_$CHROMEVER") && \
    wget -q --continue "http://chromedriver.storage.googleapis.com/$DRIVERVER/chromedriver_linux64.zip" && \
    unzip chromedriver* && \
    pwd && ls

ENV TZ Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY stream_queue.py ./
COPY startStream.sh ./
COPY gstreamer.py ./
COPY statemachine.py ./
COPY intro.webm ./
COPY docker-entrypoint.sh ./

ENTRYPOINT ["sh","docker-entrypoint.sh"]

CMD ["sh","startStream.sh" ]

