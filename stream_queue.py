import sys, argparse, time, subprocess, shlex, logging, os, io

from selenium import webdriver
from selenium.webdriver.common.keys import Keys  
from selenium.webdriver.chrome.options import Options  
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, TimeoutException

from datetime import datetime
import json
import logging
import aio_pika
import asyncio
import aiohttp

import re
import requests
from urllib.parse import urlparse
import signal
import uuid
import traceback

from statemachine import get_machine, WorkerState

selenium_timeout = 30
connect_timeout = 5

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
logger = logging.getLogger(__name__)

w = None

class Timer:
    def __init__(self, timeout, callback):
        self._timeout = timeout
        self._callback = callback
        self._task = asyncio.ensure_future(self._job())

    async def _job(self):
        await asyncio.sleep(self._timeout)
        await self._callback()

    def cancel(self):
        self._task.cancel()

class EndlessTimer(Timer):
    async def _job(self):
        while True:
            await asyncio.sleep(self._timeout)
            await self._callback()


class Streamer:
    """
    encapsulates browser controlled by selenium and gstreamer
    """

    def __init__(self):
        self.browser = None
        self.streaming_process = None
        self.chat = False
        self.audio = None
        self.video_hq = None
        self.video_mq = None
        self.video_lq = None
        self.video_file = None

    def start_browser(self):
        """
        starts browser
        """
        if self.browser:
            return
        options = Options()
        options.add_argument('--disable-infobars')
        options.add_argument('--no-sandbox')
        options.add_argument('--kiosk')
        options.add_argument('--window-size=1920,1080')
        options.add_argument('--window-position=0,0')
        options.add_experimental_option("excludeSwitches", ['enable-automation'])
        options.add_argument('--shm-size=1gb')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('--start-fullscreen')
        options.add_argument('--audio-output-sample-rate=48000')
        options.add_argument('--alsa-fixed-output-sample-rate=48000')
        options.add_argument('--no-user-gesture-required')
        options.add_argument('--autoplay-policy=no-user-gesture-required')

        logging.info('Starting browser!!')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', options=options)

    def stop_browser(self):
        """
        terminates browser
        """
        if self.browser:
            self.browser.quit()
            self.browser = None

    def join_bbb(self, join_url):
        """
        opens bbb in browser
        """
        logging.info('Open BBB and hide elements!!')
        logging.info(join_url)
        self.browser.get(join_url)

        logging.info("browser got URL")
        logging.info("waiting for overlay")

        element = EC.invisibility_of_element((By.CSS_SELECTOR, '.ReactModal__Overlay'))
        WebDriverWait(self.browser, 60).until(element)
        logging.info("wait for chat box")
        element = EC.presence_of_element_located((By.CSS_SELECTOR, '#message-input'))
        WebDriverWait(self.browser, 60).until(element)
        logging.info("start typing message")
        self.browser.find_element_by_id('message-input').send_keys("Streaming started. Viewers will see the pause video.")
        self.browser.find_elements_by_css_selector('[aria-label="Send message"]')[0].click()

        if self.chat:
            self.browser.execute_script("document.querySelector('[aria-label=\"User list\"]').parentElement.style.display='none';")
        else:
            self.browser.find_elements_by_id('chat-toggle-button')[0].click()
            self.browser.find_elements_by_css_selector('button[aria-label="Users and messages toggle"]')[0].click()
        
        self.browser.execute_script("document.querySelector('[aria-label=\"Users and messages toggle\"]').style.display='none';")
        self.browser.execute_script("document.querySelector('[aria-label=\"Options\"]').style.display='none';")
        self.browser.execute_script("document.querySelector('[aria-label=\"Actions bar\"]').style.display='none';")
        self.browser.execute_script("document.getElementById('container').setAttribute('style','margin-bottom:30px');")
        logging.info("finished join bbb")


    def hide_presentation(self):
        try:
            code = """try { document.querySelector('[aria-label="Hide presentation"]').click() } catch(e) {}"""
            self.browser.execute_script(code)
            logging.info("hide presentation")
        except:
            logger.exception("hide presentation")
        self.send_chat_message("Presentation hidden in stream. Note that this does not work when screen sharing is active")

    def restore_presentation(self):
        try:
            code = """try { document.querySelector('[aria-label="Restore presentation"]').click() } catch(e) {}"""
            self.browser.execute_script(code)
            logging.info("restore presentation")
        except:
            logger.exception("restore_presentation")
        self.send_chat_message("Presentation restored")

    def send_chat_message(self, text):
        try:
            code = """try{ require('/imports/ui/components/chat/service').default.sendGroupMessage("%s").done((p)=>{console.log(p)}) } catch(e) {} """ % text.replace("\\", "\\\\").replace('"', "\\\"")
            self.browser.execute_script(code)
            logging.info("sent message %s", text)
        except:
            logging.exception("sent message %s", text)


    def start_streaming(self):
        """
        start gstreamer
        """
        gstreamer_args = shlex.split("python3 gstreamer.py --audio {} --video-hq {} --pause-file {}".format(
            self.audio, self.video_hq,
            self.video_file,
        ))
        if self.video_mq:
            gstreamer_args.append('--video-mq')
            gstreamer_args.append(self.video_mq)
        if self.video_lq:
            gstreamer_args.append('--video-lq')
            gstreamer_args.append(self.video_lq)

        logging.info("streaming meeting... '%s'" % " ".join(gstreamer_args))
        self.streaming_process = subprocess.Popen(
            gstreamer_args, 
            stdin=subprocess.PIPE, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL,
            bufsize=1,
            universal_newlines=True,
        )

    def stop_streaming(self):
        """
        """

        logging.info("sending terminate signal to gstreamer")
        self.streaming_process.stdin.write("quit\n")
        try:
            logging.info("waiting 10s for gstreamer to terminate")
            self.streaming_process.wait(timeout=10)
        except subprocess.TimeoutExpired:
            logging.info("killing gstreamer")
            self.streaming_process.kill()
        self.streaming_process = None

    def play(self):
        if self.streaming_process:
            logger.info("sending play...")
            self.streaming_process.stdin.write("play\n")
            logger.info("play sent")
            self.send_chat_message("Now this conference is visible in the stream")

    def pause(self):
        if self.streaming_process:
            logger.info("sending pause...")
            self.streaming_process.stdin.write("pause\n")
            logger.info("pause sent")
            self.send_chat_message("Now the pause video is visible in the stream")



class Worker:
    """
    Implements communication with controller. Listens on channel for jobs
    """
    contoller_queue = 'controller'
    worker_jobs_queue = 'jobs'

    def __init__(self, amqp_url, worker_uuid, loop):
        self.state = get_machine()
        self.amqp_url = amqp_url
        self.uuid = worker_uuid
        self.loop = loop
        self.worker_queue = None
        self.rabbit_connection = None
        self.rabbit_channel = None
        self.streamer = None
        self.jobs_consumer_tag = None
        self.jobs_queue = None
        self.room_uuid = None
        self.nonce = str(uuid.uuid4())
        self.meeting_join_url = None
        self.meeting_running_url = None

        # for monitoring CPU usage
        self.last_idle = 0
        self.last_total = 0

    async def _ensure_channel(self):
        """
        checks that connection to rabbitMQ is working
        """
        if not self.rabbit_connection:
            self.rabbit_connection = await aio_pika.connect(self.amqp_url)
        if not self.rabbit_channel:
            self.rabbit_channel = await self.rabbit_connection.channel()

    async def _send_message(self, data):
        await self._ensure_channel()
        data['nonce'] = self.nonce
        data['worker'] = self.uuid
        logger.info("Sending message %s", json.dumps(data, indent=4))
        message = aio_pika.Message(
            bytes(json.dumps(data), encoding='utf-8'), 
            delivery_mode=aio_pika.DeliveryMode.PERSISTENT
        )
        await self.rabbit_channel.default_exchange.publish(
            message,
            routing_key=Worker.contoller_queue,
        )

    async def send_state(self):
        data = {
            'action': 'set_state',
            'state': self.state.state,
        }
        self._last_sent_state = self.state.state
        await self._send_message(data)

    async def register_worker(self):
        """
        send registration message to controller and register for worker queue
        """
        data = {
            'action': 'register',
        }
        await self._send_message(data)
        worker_queue_name = str(self.uuid)
        self.worker_queue = await self.rabbit_channel.declare_queue(worker_queue_name, durable=True)
        self.state.start_worker()
        await self.send_state()
        self.worker_queue_tag = await self.worker_queue.consume(
            self.on_worker_message
        )

    async def unregister_worker(self):
        data = {
            'action': 'unregister',
        }
        await self._send_message(data)
        await self.jobs_queue.cancel(self.worker_queue_tag)
        await self.worker_queue.delete()

    def validate_job_data(self, data):
        """
        checks that all required properties are available from job message
        """
        if 'streaming_targets' not in data:
            logger.error('invalid job data (missing streaming targets): %s', data)
            return False
        streaming_targets = data['streaming_targets']
        if type(streaming_targets) != list:
            logger.error('streaming targets is not a list: %s', data)
            return False
        if len(streaming_targets) == 0:
            logger.error('streaming targets must not be empty: %s', data)
            return False

        for target in streaming_targets:
            if 'janus_host' not in target:
                logger.error('invalid job target (janus_host missing): %s', target)
                return False
            if type(target.get('audio_port', None)) != int:
                logger.error('invalid job target (audio_port is not an integer): %s', target)
                return False
            if type(target.get('video_port_hq', None)) != int:
                logger.error('invalid job target (video_port_hq is not an integer): %s', target)
                return False
            if target.get('video_port_mq', None) != None:
                if type(target.get('video_port_mq', None)) != int:
                    logger.error('invalid job target (video_port_mq is not an integer): %s', target)
                    return False
            if type(target.get('video_port_lq', None)) != int:
                logger.error('invalid job target (video_port_lq is not an integer): %s', target)
                return False

        if 'meeting_join_url' not in data:
            logger.error('invalid job data (meeting_join_url missing): %s', data)
            return False
        if 'meeting_running_url' not in data:
            logger.error('invalid job data (meeting_running_url missing): %s', data)
            return False
        try:
            url = urlparse(data['meeting_join_url'])
            if url.scheme not in ['https', 'mock']:
                logger.error('invalid job data (meeting_join_url invalid): %s', data)
                return False
        except ValueError:
            logger.exception('invalid job data (meeting_join_url invalid): %s', data)
            return False

        try:
            url = urlparse(data['meeting_running_url'])
            if url.scheme not in ['https', 'mock']:
                logger.error('invalid job data (meeting_running_url invalid): %s', data)
                return False
        except ValueError:
            logger.exception('invalid job data (meeting_running_url invalid): %s', data)
            return False

        return True

    def is_meeting_running(self):
        res = requests.get(self.meeting_running_url)
        if not res.ok:
            logger.error("asking for meeting running failed: %s" % res.text)
            return False
        match = re.search("<running>(true|false)</running>", res.text, flags=re.IGNORECASE)
        if not match:
            logger.error("Invalid document: %s" % res.text)
            return False
        if match.group(1).lower() != 'true':
            logger.error("Meeting is not running")
            return False
        return True


    async def download_video(self, url):
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as resp:
                assert resp.status == 200
                with open(self.room_uuid, "wb") as fd:
                    while True:
                        chunk = await resp.content.read(4096)
                        if not chunk:
                            break
                        fd.write(chunk)


    async def handle_stream_job(self, body):
        """
        Handle method for starting a job
        first it checks for the existance of the Meeting
        if it exists, it starts a streamer
        """
        try:
            data = json.loads(body)
        except json.decoder.JSONDecodeError:
            logger.exception('unable to decode stream job message: "%s"', body)
            return False

        if not self.validate_job_data(data):
            logger.error('invalid job message: "%s"', body)
            return False

        self.meeting_running_url = data.get('meeting_running_url', None)
        self.meeting_join_url = data.get('meeting_join_url', None)
        streaming_targets = data['streaming_targets']
        audio = ",".join(["{}:{}".format(t['janus_host'], t['audio_port']) for t in streaming_targets])
        video_hq = ",".join(["{}:{}".format(t['janus_host'], t['video_port_hq']) for t in streaming_targets])
        video_mq = ",".join(["{}:{}".format(t['janus_host'], t['video_port_mq']) for t in streaming_targets])
        video_lq = ",".join(["{}:{}".format(t['janus_host'], t['video_port_lq']) for t in streaming_targets])
        self.room_uuid = data.get('room', None)
        url = data.get('pause_url', None)

        # notify controller that we are preparing for streaming
        self.state.new_job()
        await self.send_state()
        
        # watch BBB room state and launch chrome
        # this is done as a background task, so this happens in parallel to
        # downloading the pause loop video
        self.bbb_meeting_watcher = self.loop.create_task(self.watch_bbb_meeting())

        self.streamer = Streamer()
        self.streamer.audio = audio
        self.streamer.video_hq = video_hq
        self.streamer.video_mq = video_mq
        self.streamer.video_lq = video_lq
        if url:
            try:
                self.streamer.video_file = self.room_uuid
                await self.download_video(url)
            except:
                logger.exception("failed to download pause video")
                self.streamer.video_file = 'intro.webm'
        else:
            self.streamer.video_file = 'intro.webm'
        self.streamer.start_streaming()
        self.state.streaming_started()
        await self.send_state()

        #self.streamer.join_bbb(meeting_join_url)
        return True

    async def watch_bbb_meeting(self):
        """
        watch BBB Meeting URL if meeting is running and switch state of pause/meeting stream
        """
        while True:
            try:
                meeting_running = self.is_meeting_running()
            except Exception as e:
                logger.exception("exception while trying to query bbb api")
                await asyncio.sleep(5.0)
                continue
            logger.info("Current state of meeting_running: %s. State: %s", meeting_running, self.state.state)
            if meeting_running:
                if self.state.state == WorkerState.BBB_INACTIVE_STREAMING:
                    await self.unpause_streaming()
                if self.state.state in (WorkerState.BBB_INACTIVE_PAUSED, WorkerState.BBB_INACTIVE_STREAMING):
                    logger.info("start browser")
                    self.streamer.start_browser()
                    logger.info("join bbb")
                    try:
                        self.streamer.join_bbb(self.meeting_join_url)
                    except:
                        logger.exception("failed to join BBB room")
                        self.streamer.stop_browser()
                        continue
                    logger.info("change room state")
                    self.state.room_active()
                    logger.info("send state")
                    await self.send_state()
            else:
                if self.state.state == WorkerState.BBB_ACTIVE_STREAMING:
                    await self.pause_streaming()
                if self.state.state in (WorkerState.BBB_ACTIVE_PAUSED, WorkerState.BBB_ACTIVE_STREAMING):
                    self.streamer.stop_browser()
                    self.state.room_terminated()
                    await self.send_state()
            logger.info("Current state: %s", self.state.state)
            await asyncio.sleep(5.0)

    async def terminate_stream(self):
        """
        stops gstreamer and browser
        """
        if self.streamer:
            self.streamer.stop_streaming()
            self.streamer.stop_browser()
            self.streamer = None
        self.meeting_join_url = None
        self.meeting_running_url = None
        if self.bbb_meeting_watcher:
            self.bbb_meeting_watcher.cancel()
            self.bbb_meeting_watcher = None
        await self.send_state()
        await self.register_jobs_channel()

    async def pause_streaming(self):
        logger.info("pause")
        if self.streamer:
            self.streamer.pause()

    async def unpause_streaming(self):
        logger.info("unpause")
        if self.streamer:
            self.streamer.play()

    async def on_worker_message(self, message):
        """
        behandelt Nachrichten aus der Queue für den Worker
        """
        logger.info("received worker message: %s", message.body.decode("utf-8"))
        try:
            data = json.loads(message.body)
        except json.JSONDecodeError:
            await message.ack()
            return
        if data.get('nonce', None) != self.nonce:
            logger.info("nonce does not match")
            await message.ack()
            return
        action = data.get('action', None)
        if not action:
            logger.info("dropping message without action")
            await message.ack()
            return
        try:
            if action == 'start_streaming':
                successful = await self.handle_stream_job(message.body)
                await message.ack()
                await self.send_state()
                return
            if action == 'stop_streaming':
                await self.terminate_stream()
                self.state.stop_streaming()
                await message.ack()
                await self.send_state()
                return
            if action == 'pause_streaming':
                await self.pause_streaming()
                self.state.pause()
                await message.ack()
                await self.send_state()
                return
            if action == 'unpause_streaming':
                await self.unpause_streaming()
                self.state.unpause()
                await message.ack()
                await self.send_state()
                return
            if action == 'hide_presentation':
                if self.streamer:
                    self.streamer.hide_presentation()
                await message.ack()
                return
            if action == 'restore_presentation':
                if self.streamer:
                    self.streamer.restore_presentation()
                await message.ack()
                return
            if action == 'send_chat_message':
                text = data.get("text", None)
                if self.streamer and text:
                    self.streamer.send_chat_message(text)
                await message.ack()
                return
            if action == 'status':
                pass # TODO
            logger.error("unhandled message: %s", message.body.decode("utf-8"))
            await message.ack()
        except:
            logger.exception("error handling message")

    async def on_job_message(self, message):
        """
        behandelt Nachrichten aus der Queue für alle Worker.
        Dort werden nur neue Jobs verteilt.
        """
        logger.info("received job FROM JOB QUEUE: %s", message.body.decode("utf-8"))
        if not self.state.state == WorkerState.IDLE:
            logger.error("Worker is not in expected state %s. It is %s instead", WorkerState.IDLE, self.state.state)
            await self.emergency_exit()
            return

        await self.unregister_jobs_channel()
        successful = await self.handle_stream_job(message.body)
        logger.info("Handling of job result: %s", successful)
        if successful:
            await self.send_streaming_started()
        else:
            await self.send_streaming_stopped()
            self.register_jobs_channel()
        await message.ack()
        await self.send_state()


    async def register_jobs_channel(self):
        """
        register for consuming jobs
        """
        logger.info("registering jobs channel: %s", Worker.worker_jobs_queue)
        self.jobs_queue = await self.rabbit_channel.declare_queue(Worker.worker_jobs_queue, durable=True)
        self.jobs_consumer_tag = await self.jobs_queue.consume(self.on_job_message)
        logger.info("consumer tag: %s", self.jobs_consumer_tag)

    async def unregister_jobs_channel(self):
        """
        no longer consume jobs
        """
        logger.info("unregistering jobs channel with tag: %s", self.jobs_consumer_tag)
        if self.jobs_consumer_tag:
            await self.jobs_queue.cancel(self.jobs_consumer_tag)
            self.jobs_consumer_tag = None

    def get_cpu_usage(self):
        with open('/proc/stat') as f:
            fields = [float(column) for column in f.readline().strip().split()[1:]]
        idle, total = fields[3], sum(fields)
        idle_delta, total_delta = idle - self.last_idle, total - self.last_total
        self.last_idle, self.last_total = idle, total
        utilisation = (1.0 - idle_delta / total_delta)
        return utilisation

    async def send_ping(self):
        try:
            cpu_usage = self.get_cpu_usage()
        except:
            cpu_usage = 0.0
            logger.excpetion("failed to get cpu")

        data = {
            'action': 'ping',
            'cpu_usage': cpu_usage,
        }
        if self._last_sent_state != self.state.state:
            logger.warn("Last sent state to controller is %s, should be %s" % (self._last_sent_state, self.state.state))
        else:
            logger.info("Last sent state is ok")
        await self._send_message(data)

    async def emergency_exit(self):
        await self.unregister_worker()
        raise RuntimeError("emergency exit")

    async def run(self):
        """
        run worker. This method will block until pika consuming terminates
        """
        await self._ensure_channel()
        await self.rabbit_channel.set_qos(prefetch_count=1)
        await self.register_worker()
        await self.register_jobs_channel()
        EndlessTimer(10, self.send_ping)

def handle_exception(loop, context):
    # context["message"] will always be there; but context["exception"] may not
    msg = context.get("exception", context["message"])
    logging.error(f"Caught unhandled exception: {msg}")
    trace = io.StringIO()
    context['future'].print_stack(file=trace)
    logging.error(trace.getvalue())
    logging.info("Shutting down...")
    asyncio.create_task(shutdown(loop))

async def shutdown(loop, signal=None):
    """Cleanup tasks tied to the service's shutdown."""
    if signal:
        logging.info(f"Received exit signal {signal.name}...")
    if w:
        try:
            await w.unregister_worker()
        except:
            logging.exception("failed to stop worker")
    logging.info("Closing database connections")
    tasks = [t for t in asyncio.all_tasks() if t is not
             asyncio.current_task()]

    [task.cancel() for task in tasks]

    logging.info(f"Cancelling {len(tasks)} outstanding tasks")
    await asyncio.gather(*tasks, return_exceptions=True)
    logging.info(f"Flushing metrics")
    loop.stop()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--amqp_url', required=True, help='URL to connect to RabbitMQ broker')
    parser.add_argument('-w', '--worker_uuid', required=True, help='UUID this worker is registered with')
    args = parser.parse_args()
    loop = asyncio.get_event_loop()
    signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
    for s in signals:
        loop.add_signal_handler(
            s, lambda s=s: asyncio.create_task(shutdown(loop, signal=s)))
    loop.set_exception_handler(handle_exception)
    w = Worker(args.amqp_url, args.worker_uuid, loop)
    loop.create_task(w.run())
    try:
        loop.run_forever()
    except:
        loop.run_until_complete(w.unregister_worker())
        for task in asyncio.Task.all_tasks():
            task.cancel()
    finally:
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()
