from transitions import Machine

class WorkerState:
    INITIAL                = 'initial'
    IDLE                   = 'idle'
    FAILED                 = 'failed'
    BBB_INACTIVE_PREPARE_STREAMING  = 'bbb_inactive_prepare_streaming'
    BBB_ACTIVE_PREPARE_STREAMING    = 'bbb_active_prepare_streaming'
    BBB_INACTIVE_PAUSED    = 'bbb_inactive_paused'
    BBB_ACTIVE_PAUSED      = 'bbb_active_paused'
    BBB_ACTIVE_STREAMING   = 'bbb_active_streaming'
    BBB_INACTIVE_STREAMING = 'bbb_inactive_streaming'
    STOPPED                = 'stopped'
    

def get_machine():
    states = [
        WorkerState.INITIAL,
        WorkerState.IDLE,
        WorkerState.BBB_INACTIVE_PREPARE_STREAMING,
        WorkerState.BBB_ACTIVE_PREPARE_STREAMING,
        WorkerState.BBB_INACTIVE_PAUSED,
        WorkerState.BBB_ACTIVE_PAUSED,
        WorkerState.BBB_ACTIVE_STREAMING,
        WorkerState.BBB_INACTIVE_STREAMING,
        WorkerState.STOPPED,
    ]
    ws = WorkerState()
    m = Machine(model=ws, states=states, initial=WorkerState.INITIAL)
    m.add_transition('start_worker', WorkerState.INITIAL, WorkerState.IDLE)
    m.add_transition('new_job', WorkerState.IDLE, WorkerState.BBB_INACTIVE_PREPARE_STREAMING)
    m.add_transition('stop_streaming', [
        WorkerState.BBB_INACTIVE_PREPARE_STREAMING,
        WorkerState.BBB_ACTIVE_PREPARE_STREAMING,
        WorkerState.BBB_INACTIVE_PAUSED,
        WorkerState.BBB_ACTIVE_PAUSED,
        WorkerState.BBB_ACTIVE_STREAMING,
        WorkerState.BBB_INACTIVE_STREAMING,
    ], WorkerState.IDLE)
    m.add_transition('stop_worker', [
        WorkerState.IDLE,
        WorkerState.BBB_INACTIVE_PREPARE_STREAMING,
        WorkerState.BBB_ACTIVE_PREPARE_STREAMING,
        WorkerState.BBB_INACTIVE_PAUSED,
        WorkerState.BBB_ACTIVE_PAUSED,
        WorkerState.BBB_ACTIVE_STREAMING,
    ], WorkerState.STOPPED)
    
    m.add_transition('room_active', WorkerState.BBB_INACTIVE_PAUSED, WorkerState.BBB_ACTIVE_PAUSED)
    m.add_transition('room_terminated', WorkerState.BBB_ACTIVE_PAUSED, WorkerState.BBB_INACTIVE_PAUSED)
    m.add_transition('room_active', WorkerState.BBB_INACTIVE_STREAMING, WorkerState.BBB_ACTIVE_STREAMING)
    m.add_transition('room_terminated', WorkerState.BBB_ACTIVE_STREAMING, WorkerState.BBB_INACTIVE_STREAMING)
    m.add_transition('room_active', WorkerState.BBB_INACTIVE_PREPARE_STREAMING, WorkerState.BBB_ACTIVE_PREPARE_STREAMING)
    m.add_transition('room_terminated', WorkerState.BBB_ACTIVE_PREPARE_STREAMING, WorkerState.BBB_INACTIVE_PREPARE_STREAMING)

    m.add_transition('streaming_started', WorkerState.BBB_INACTIVE_PREPARE_STREAMING, WorkerState.BBB_INACTIVE_PAUSED)
    m.add_transition('streaming_started', WorkerState.BBB_ACTIVE_PREPARE_STREAMING, WorkerState.BBB_ACTIVE_PAUSED)

    m.add_transition('pause', WorkerState.BBB_INACTIVE_STREAMING, WorkerState.BBB_INACTIVE_PAUSED)
    m.add_transition('pause', WorkerState.BBB_ACTIVE_STREAMING,   WorkerState.BBB_ACTIVE_PAUSED)
    m.add_transition('unpause', WorkerState.BBB_ACTIVE_PAUSED, WorkerState.BBB_ACTIVE_STREAMING)
    return ws
